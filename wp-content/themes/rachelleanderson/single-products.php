<?php
/**
 * The template for displaying all single posts.
 *
 * @package rachelle_anderson
 */

$recipe_loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1, 'tag' => $post->post_name, 'meta_key' => '', 'orderby' => 'meta_value', 'order' => 'DESC' ) );

$products_loop = new WP_Query( array( 'post_type' => 'products', 'posts_per_page' => -1, 'meta_key' => '', 'orderby' => 'meta_value', 'order' => 'ASC' ) );

$results = showAvgRating( $post->ID );

get_header(); ?>

	<div class="template product-detail" data-menu-item="products">
		<div class="header-padding">
			<section class="product-info">
				<div class="inner-container">
					<div class="section-heading hide-desktop">
						<h1 class="section-heading-text"><?php the_title() ?></h1>
						<div class="write-review">
							<?php echo $results[0]; ?>
							<p class="link-reviews js-to-anchor">Write a Review</p>
						</div>
					</div>
					<div class="column-container">
						<div class="column-half product-images js-product-images">
							<?php 
							$images = get_field( 'product_images' );
							if( $images ): ?>
								<?php foreach( $images as $image ): ?>
									<div>
										<div class="product-image" style="background: url( '<?php echo $image['url'] ?>' ) center center no-repeat; background-size: contain;">
										</div>
										<img src="<?php echo $image['url']; ?>" class="hide-desktop" alt="<?php echo $image['alt']; ?>" />
									</div>
								<?php endforeach; ?>
							<?php endif; ?>
						</div>
						<div class="column-half">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php get_template_part( 'template-parts/content', 'single-products' ); ?>
							<?php endwhile; // End of the loop. ?>
							<a href="/contact#where-to-buy" class="button large">
								<span>Where to Buy</span>
							</a>
						</div>
					</div>
					<div class="how-to-cook">
						<div class="cooking-options">
							<h3 class="burford-text">How to Cook - </h3>
							<?php if ( get_field( 'grill_instructions' ) ) { ?>
								<h4 class="burford-text js-cooking active-cook" data-cook="grill-instructions">Grill</h4>
							<?php }
							if ( get_field( 'skillet_instructions' ) ) { ?>
								<h4 class="burford-text js-cooking" data-cook="skillet-instructions">Skillet</h4>
							<?php } ?>
						</div>
						<div class="cooking-instructions">
							<?php if ( get_field( 'grill_instructions' ) ) { ?>
								<div class="grill-instructions js-instruct"><?php the_field( 'grill_instructions' ); ?></div>
							<?php }
							if ( get_field( 'skillet_instructions' ) ) { ?>
								<div class="skillet-instructions js-instruct"><?php the_field( 'skillet_instructions' ); ?></div>
							<?php } ?>
						</div>
					</div>
				</div>
			</section>
			<section class="other-varieties">
				<div class="inner-container">
					<div class="section-heading">
						<h2 class="section-heading-text">Other Varieties</h2>
					</div>
					<div class="column-container">
						<?php if ( have_posts() ) :
							while ( $products_loop->have_posts() ) : $products_loop->the_post(); ?>
								<?php get_template_part( 'template-parts/content', 'product-preview' ); ?>
							<?php endwhile;
						endif; 
						wp_reset_postdata(); ?>
					</div>
				</div>
			</section>
			<section class="recipes">
				<div class="inner-container slider-arrows">
					<div class="section-heading">
						<h2 class="section-heading-text">Recipes</h2>
					</div>
					<div class="column-container grub-slider js-recipe-slider">
						<?php if ( have_posts() ) :
							while ( $recipe_loop->have_posts() ) : $recipe_loop->the_post(); ?>
								<div class="column-third">
									<a href="<?php the_permalink(); ?>">
										<?php $src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
										<div class="recipe-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
									</a>
									<div class="recipe-preview">
										<a href="<?php the_permalink(); ?>">
											<h3 class="burford-text blue"><?php the_title(); ?></h3>
										</a>
										<div class="post-preview"><?php the_excerpt(); ?></div>
									</div>
										<a href="<?php the_permalink(); ?>" class="button">
											<span>View Recipe</span>
										</a>
								</div>
							<?php endwhile;
						endif; 
						wp_reset_postdata(); ?>
					</div>
				</div>
			</section>
			<section class="reviews js-anchor">
				<div class="inner-container">
					<div class="section-heading">
						<h2 class="section-heading-text">Reviews</h2>
					</div>
					<div class="write-review">
						<?php echo $results[1]; ?>
					</div>
					<a href="javascript:void(0)" class="button js-toggle-review-field">
						<span>Add Your Review</span>
					</a>
					<div class="review-form">
						<?php 
							$args = array(
								'label_submit' => 'Submit',
								'comment_field' => '<p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" placeholder="Review*" aria-required="true" tabindex="5"></textarea></p>',
							);
							comment_form( $args ); 
						?>
					</div>
					<div class="column-container list-reviews" data-review-offset="0">
						<?php 
							$args = array(
								'order' => 'DESC',
								'status' => 'approve',
								'post_id' => $post->ID,
								'number' => '3',
								'offset' => '0',
							);

							// The Query
							$comments_query = new WP_Comment_Query;
							$comments = $comments_query->query( $args );

							echo comments_number();

							// Comment Loop
							if ( $comments ) {
								foreach ( $comments as $comment ) { 
									$titleArray = get_comment_meta( $comment->comment_ID, 'title', $single = false ); 
									$ratingArray = get_comment_meta( $comment->comment_ID, 'rating', $single = false ); 
									$lastNameArray = get_comment_meta( $comment->comment_ID, 'last', $single = false ); ?>
									<div class="column-third">
										<h3 class="burford-text blue"><?php echo $titleArray[0] ?></h3>
										<div class="star-rating">
											<?php for ( $x = 1; $x <= 5; $x++ ) { 
												if ( $x <= $ratingArray[0] ) { ?>
													<div class="star full"></div>
												<?php } else { ?>
													<div class="star"></div>
												<?php } 
											}?>
										</div>
										<p class="reviewer"><?php echo $comment->comment_author ?> <?php echo $lastNameArray[0] ?> <span><?php comment_date('n-j-y'); ?></span></p>
										<p><?php echo $comment->comment_content ?></p>
									</div>
								<?php } ?>
							<?php } else {
								echo '<p>No reviews found.</p>';
							} 
						?>
					</div>
					<a href="javascript:void(0)" class="button js-load-reviews">
						<span>View More</span>
					</a>
				</div>
			</section>
		</div>
	</div>

<script>
	var ajaxurl = <?php echo '"' . admin_url( 'admin-ajax.php' ) . '";'; ?>
</script>

<?php get_footer(); ?>
