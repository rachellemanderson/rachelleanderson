<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package rachelle_anderson
 */

get_header(); ?>

	<div class="not-found">
		<section class="hero-image no-space">
			<div class="over-text header-padding">
				<div class="outer-table">
					<div class="inner-table">
						<div class="section-heading">
							<h1 class="section-heading-text">404 Error</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="yellow-banner">
			<p class="container">Looking for Grub Love in all the wrong places? Try our <a href="<?php echo esc_url( home_url( '/' ) ); ?>">homepage</a> instead.</p>
		</section>
	</div>

<?php get_footer(); ?>
