<?php
/**
 * @package grub-gourmet
 */
?>

<!DOCTYPE html>
<!--[if lte IE 8]> <html class="no-js lt-ie10 lt-ie9 oldie" <?php language_attributes() ?>> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 oldie" <?php language_attributes() ?>> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" <?php language_attributes() ?>> <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js">

	<link href="https://fonts.googleapis.com/css?family=Cedarville+Cursive|Oswald:400,700|Raleway:200,400,600" rel="stylesheet">

	<!-- Meta Tags for sharing to Facebook -->
	<meta property="og:url" content="<?php the_permalink(); ?>" />
	<meta property="og:title" content="<?php the_title(); ?>" />
	<meta property="og:description" content="#sharegrub" />
	<meta property="og:image" content="<?php the_post_thumbnail_url(); ?>" />
	<?php 
		$url = get_the_post_thumbnail_url();
		$secure_url = preg_replace("/^http:/i", "https:", $url); 
	?>
	<meta property="og:image:secure_url" content="<?php echo $secure_url; ?>" />
	<meta property="og:image:width" content="200" /> 
	<meta property="og:image:height" content="auto" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-asset-url="<?php echo get_template_directory_uri();?>">
	<div class="breakpoint-context"></div>

	<div id="page">
		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'rachelle_anderson' ); ?></a>
		<header class="site-header">	
			<div class="header-bar">
				<div class="container">
					<nav class="main-navigation" role="navigation">
						<?php
							wp_nav_menu();
						?>
					</nav>
				</div>
			</div>
		</header>

		<div id="content" class="site-content">
