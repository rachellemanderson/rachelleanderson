<?php
/**
 * @package rachelle_anderson
 */
?>

		</div><!-- #content -->

		<footer class="site-footer">
			<div class="container">
				<div class="social-icons">
					<a href="#" target="_blank"><i class="fa fa-bitbucket" aria-hidden="true"></i></a>
					<a href="https://www.linkedin.com/in/rachelle-anderson-3831b4a2" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					<a href="https://www.instagram.com/_rachellemarie/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
					<a href="https://www.facebook.com/rachelle.dietz" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
				</div>
				<!-- <p>&copy;<?php bloginfo( 'name' ); ?> <script>document.write(new Date().getFullYear())</script></p> -->
			</div>
		</footer>

	</div><!-- #page -->

	<?php wp_footer(); ?>

	<!-- Google Tag Manager -->
		<!-- INSET GTM TAG -->
	<!-- End Google Tag Manager -->

</body>

</html>
