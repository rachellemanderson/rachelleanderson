<?php
/**
 * Template for displaying search forms 
 *
 * @package rachelle_anderson
 */
?>

<form role="search" method="get" class="search-form js-trigger-filters" action="<?php echo esc_url( home_url( '/' ) ); ?>" data-filter="search">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'rachelle_anderson' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search', 'placeholder', 'rachelle_anderson' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	</label>
	<button type="submit" class="search-submit"><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'rachelle_anderson' ); ?></span></button>
</form>
