<?php
/**
 * The template for displaying all single posts.
 *
 * @package rachelle_anderson
 */

get_header(); ?>

	<?php while ( have_posts() ) : the_post(); ?>
		<div class="template blog-single" data-menu-item="blog">
			<section class="no-space">
				<?php if ( has_post_thumbnail() ) {
					$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
					<div class="hero-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
				<?php } else { ?>
					<div class="header-padding"></div>
				<?php } ?>
			</section>
			<section class="single-post-content">
				<div class="inner-container">
					<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>" class="all-posts">
						<img src="<?php echo get_template_directory_uri();?>/assets/img/arrow.png" alt=""/>
						<p>All Posts</p>
					</a>
					<?php get_template_part( 'template-parts/content', 'single' ); ?>
				</div>
			</section>
			<?php echo get_post_meta( $post->ID, 'test_custom', true ); ?>
		</div>
	<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
