<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rachelle_anderson
 */

get_header(); ?>

	<div class="template blog-archive" data-blog-url="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>" data-menu-item="blog">
		<section class="blog-hero no-space vertical-referance">
			<div class="container">
				<div class="hero-image" style="background: url( '<?php echo get_template_directory_uri(); ?>/assets/img/hero-blog.jpg' ) center center no-repeat; background-size: cover;"></div>
				<div class="over-text">
					<div class="outer-table">
						<div class="inner-table">
							<h1 class="heading"><?php the_archive_title(); ?></h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="blog-content">
			<div class="inner-container">
				<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php 
							get_template_part( 'template-parts/content', get_post_format() );
						?>
					<?php endwhile; ?>
				<?php else : ?>
					<?php get_template_part( 'template-parts/content', 'none' ); ?>
				<?php endif; ?>
			</div>
		</section>
		<!-- <section class="pagination">
			<div class="inner-container">
				<div class="column-page">
					<?php 
						$total_posts = $blog_loop->found_posts;
						$num_pages = ceil( $total_posts / $posts_per_page );
						$max_page_nums = 5;
						$page_margin = $max_page_nums - ( ( $max_page_nums + 1 ) / 2 );
						$current_page = $post_page_num[0];

						function displayPageLinks( $j, $current_page ) {
							if ( $j == $current_page ) {
								echo '<a class="page-number active js-trigger-click-filters" href="javascript:void(0)" data-filter="page">' . $j . '</a>';
							} else {
								echo '<a class="page-number js-trigger-click-filters" href="javascript:void(0)" data-filter="page">' . $j . '</a>';
							}
						}

						// If there is overflow of pages on both sides of the current page
						if ( ( $current_page - $page_margin ) > 2 && ( $current_page + $page_margin ) < ( $num_pages - 1 ) ) {
							echo '<a class="page-number js-trigger-click-filters" href="javascript:void(0)" data-filter="page">1</a><p>...</p>';
							for ( $j = ( $current_page - $page_margin ); $j <= ( $current_page + $page_margin ); $j++ ) {
								displayPageLinks( $j, $current_page );
							}
							echo '<p>...</p><a class="page-number js-trigger-click-filters" href="javascript:void(0)" data-filter="page">' . $num_pages . '</a>';
						} 
						// If the current page is within the maximun number of pages on the upper end (overflow on low end)
						elseif ( ( $current_page - $page_margin ) > 2 && $num_pages > ( $max_page_nums + 2 ) ) {
							echo '<a class="page-number js-trigger-click-filters" href="javascript:void(0)" data-filter="page">1</a><p>...</p>';
							for ( $j = ( $num_pages - $max_page_nums + 1 ); $j <= $num_pages; $j++ ) {
								displayPageLinks( $j, $current_page );
							}
						} 
						// If the current page is within the maximun number of pages on the lower end (overflow on high end)
						elseif ( ( $current_page + $page_margin ) < ( $num_pages - 1 ) && $num_pages > ( $max_page_nums + 2 ) ) { 
							for ( $j = 1; $j <= $max_page_nums; $j++ ) {
								displayPageLinks( $j, $current_page );
							}
							echo '<p>...</p><a class="page-number js-trigger-click-filters" href="javascript:void(0)" data-filter="page">' . $num_pages . '</a>';
						} 
						// If the total number of pages is less than or equal to the maximun number of pages + 2 (account for first and last page)
						elseif ( $num_pages > 1 ) {
							for ( $j = 1; $j<= $num_pages; $j++ ) {
								displayPageLinks( $j, $current_page );
							}
						}
					?>
				</div>
			</div>
		</section> -->
	</div>

<?php get_footer(); ?>