<?php 

function showAvgRating( $postid ) {
	$args = array(
		'order' => 'DESC',
		'status' => 'approve',
		'post_id' => $postid,
	);

	// The Query
	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query( $args );
	$ratingTotal = 0;
	$numReviews = 0;

	$htmlOut = array( '', '' );

	// Comment Loop
	if ( $comments ) {
		foreach ( $comments as $comment ) { 
			$ratingArray = get_comment_meta( $comment->comment_ID, 'rating', $single = false ); 
			$numReviews += 1;
			$ratingTotal += $ratingArray[0];
		} 
	} else {
		$htmlOut[1].= '<p>Be the first to add a review.</p>';
	} 

	// Overall average rating and average rating rounded up to the nearest half
	if ( $numReviews > 0 ) {
		$ratingAvg = $ratingTotal / $numReviews;
		$reviewsRoundedAvg = ceil( $ratingAvg * 2 ) / 2;
		$htmlOut[0].= '<div class="star-rating">';
		$htmlOut[1].= '<div class="star-rating">';
			for ( $y = 1; $y <= 5; $y++ ) { 
				if ( $y <= $reviewsRoundedAvg ) { 
					$htmlOut[0].= '<div class="star full"></div>';
					$htmlOut[1].= '<div class="star full"></div>';
				} elseif ( ( $y - $reviewsRoundedAvg ) === .5 ) {
					$htmlOut[0].= '<div class="star half"></div>';
					$htmlOut[1].= '<div class="star half"></div>';
				} else {
					$htmlOut[0].= '<div class="star"></div>';
					$htmlOut[1].= '<div class="star"></div>';
				} 
			}
		
		$htmlOut[0].= '</div>';
		$htmlOut[1].= '</div>';

		// shouwtext = true
		$htmlOut[1].= '<p class="total-reviews">(<span>' . $numReviews . '</span> review';

		if( $numReviews > 1 ) { 
			$htmlOut[1].= 's';  
		}

		$htmlOut[1].= ')</p>';
	}

	return $htmlOut;

}