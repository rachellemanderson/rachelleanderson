<?php
/**
 * Using Wordpress comments for reviews
 */
add_filter( 'comment_form_default_fields', 'custom_fields' );

function custom_fields( $fields ) {

	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$fields[ 'author' ] = '<p class="comment-form-author">'.
		'<label for="author">' . __( 'First Name' ) . '</label>'.
		( $req ? '<span class="required">*</span>' : '' ).
		'<input id="author" name="author" type="text" placeholder="First Name*" value="' . esc_attr( $commenter[ 'comment_author' ] ) .
		'" size="30" tabindex="2"' . $aria_req . ' /></p>';

	$fields[ 'last' ] = '<p class="comment-form-last">'.
		'<label for="last">' . __( 'Last Name' ) . '</label>'.
		( $req ? '<span class="required">*</span>' : '' ).
		'<input id="last" name="last" type="text" placeholder="Last Name*" value="' . esc_attr( $commenter[ 'comment_author_url' ] ) .
		'" size="30" tabindex="3"' . $aria_req . ' /></p>';

  return $fields;
}

add_action( 'comment_form_logged_in_before', 'rating_field' );
add_action( 'comment_form_before_fields', 'rating_field' );

function rating_field () {
	echo '<p class="comment-form-rating">'.
	'<label class="main-label" for="rating">'. __('Rating') . '<span class="required">*</span></label>
	<span class="commentratingbox">';

	//Current rating scale is 1 to 5. If you want the scale to be 1 to 10, then set the value of $i to 10.
	for( $i=1; $i <= 5; $i++ )
	echo '<span class="commentrating"><label for="'. $i .'"></label><input type="radio" name="rating" id="'. $i .'" value="'. $i .'"/>'. $i .'</span>';

	echo'</span></p>';
}

add_action( 'comment_form_logged_in_after', 'title_field' );
add_action( 'comment_form_after_fields', 'title_field' );

function title_field () {
	echo '<p class="comment-form-title">'.
	'<label for="title">' . __( 'Comment Title' ) . '</label>'.
	'<input id="title" name="title" type="text" size="30"  tabindex="4" placeholder="Review Title*" /></p>';
}

// Save the comment meta data along with comment

add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
	if ( ( isset( $_POST['last'] ) ) && ( $_POST[ 'last' ] != '' ) )
		$last = wp_filter_nohtml_kses($_POST[ 'last' ]);
		add_comment_meta( $comment_id, 'last', $last );

	if ( ( isset( $_POST['title'] ) ) && ( $_POST[ 'title' ] != '' ) )
		$title = wp_filter_nohtml_kses( $_POST[ 'title' ] );
		add_comment_meta( $comment_id, 'title', $title );

	if ( ( isset( $_POST['rating'] ) ) && ( $_POST[ 'rating' ] != '' ) )
		$rating = wp_filter_nohtml_kses( $_POST[ 'rating' ] );
		add_comment_meta( $comment_id, 'rating', $rating );
}

add_filter( 'comment_text', 'modify_comment' );
function modify_comment( $text ){

  $plugin_url_path = WP_PLUGIN_URL;

	if( $commenttitle = get_comment_meta( get_comment_ID(), 'title', true ) ) {
		$commenttitle = '<strong>' . esc_attr( $commenttitle ) . '</strong><br/>';
		$text = $commenttitle . $text;
	} 

	if( $commentrating = get_comment_meta( get_comment_ID(), 'rating', true ) ) {
		$commentrating = '<p class="comment-rating">  <img src="'. $plugin_url_path .
		'/ExtendComment/images/'. $commentrating . 'star.gif"/><br/>Rating: <strong>'. $commentrating .'</strong></p>';
		$text = $commentrating . $text;
		return $text;
	} else {
		return $text;
	}
}

function wpb_move_comment_field_to_bottom( $fields ) {
	$comment_field = $fields[ 'comment' ];
	unset( $fields[ 'comment' ] );
	$fields[ 'comment' ] = $comment_field;

	if(isset($fields['email']))
		unset($fields['email']);

	if(isset($fields['url']))
		unset($fields['url']);

	return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

function change_submit_button( $submit_field ) {
$changed_submit = str_replace ( 'name="submit" type="submit" id="submit"', 'name="submit" type="submit" id="submit" tabindex = "5"', $submit_field );
    return $changed_submit;
}
add_filter( 'comment_form_submit_field', 'change_submit_button' );

/**
 * Load more reviews
 */

add_action( 'wp_ajax_getmorereviews', 'test_getmorereviews' );
add_action( 'wp_ajax_nopriv_getmorereviews', 'test_getmorereviews' );

function test_getmorereviews() {
	$results = [];
	$offset = $_REQUEST[ 'offset' ];
	$postID = $_REQUEST[ 'postID' ];

	$args = array(
		'order' => 'DESC',
		'status' => 'approve',
		'post_id' => $postID,
		'number' => 3,
		'offset' => $offset,
	);

	// The Query
	$comments_query = new WP_Comment_Query;
	$comments = $comments_query->query( $args );

	if ( $comments ) {
		foreach ( $comments as $comment ) { 
			$review = [];
			$review[] = $comment;
			$review[] = get_comment_meta( $comment->comment_ID, 'title', $single = false ); 
			$review[] = get_comment_meta( $comment->comment_ID, 'rating', $single = false ); 
			$review[] = get_comment_meta( $comment->comment_ID, 'last', $single = false );
			$review[] = get_comment_date( 'n-j-y', $comment->comment_ID );
			$results[] = $review;
		} 
	}

	// Serve results
	echo json_encode( $results );

	wp_die();
}
