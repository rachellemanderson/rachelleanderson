<?php 

/**
 * Load close locations
 */

add_action( 'wp_ajax_getlocations', 'getlocations' );
add_action( 'wp_ajax_nopriv_getlocations', 'getlocations' );

function getlocations() {
	$center_lat = $_REQUEST[ 'centerlat' ];
	$center_lng = $_REQUEST[ 'centerlng' ];
	$radius = $_REQUEST[ 'radius' ];
	$matched_locations = [];

	// Start XML file, create parent node
	$dom = new DOMDocument( '1.0' );
	$node = $dom->createElement( 'markers' );
	$parnode = $dom->appendChild( $node );

	header( 'Content-type: text/xml' );

	$locations_loop = new WP_Query( array( 'post_type' => 'locations' ) );

	if ( $locations_loop->have_posts() ) {
		while ( $locations_loop->have_posts() ) : $locations_loop->the_post(); 
			$lat = get_field( 'location_lat' );
			$lng = get_field( 'location_lng' );
			$distance = ( 3959 * acos( cos( deg2rad( $center_lat ) ) * cos( deg2rad( $lat ) ) * cos( deg2rad( $lng ) - deg2rad( $center_lng ) ) + sin( deg2rad( $center_lat ) ) * sin( deg2rad( $lat ) ) ) );

			if ( $distance <= $radius ) {

				$location = [];
				$location[ 'name' ] = get_the_title();
				$location[ 'address' ] = get_field( 'location_address' );
				$location[ 'lat' ] = $lat;
				$location[ 'lng' ] = $lng;
				$location[ 'distance' ] = $distance;
				$matched_locations[  ] = $location;
			}
		endwhile; 
	}

	function sortByOrder( $a, $b ) {
		return $a[ 'distance' ] > $b[ 'distance' ];
	}
	usort( $matched_locations, 'sortByOrder');

	foreach ( $matched_locations as $matched_location ) {
	    $node = $dom->createElement( 'marker' );
	    $newnode = $parnode->appendChild( $node );
	    $newnode = $parnode->appendChild( $node );
	    $newnode->setAttribute( 'name', $matched_location[ 'name' ] );
	    $newnode->setAttribute( 'address', $matched_location[ 'address' ] );
	    $newnode->setAttribute( 'lat', $matched_location[ 'lat' ] );
	    $newnode->setAttribute( 'lng', $matched_location[ 'lng' ] );
	    $newnode->setAttribute( 'distance', $matched_location[ 'distance' ] );
	}

	echo $dom->saveXML();

	wp_die();
}