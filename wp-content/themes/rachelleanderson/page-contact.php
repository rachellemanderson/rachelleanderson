<?php
/**
 * Template Name: Contact
 *
 * @package rachelle_anderson
 */

get_header(); ?>


	<div class="template contact" data-menu-item="contact">
		<section class="no-space">
			<?php if ( has_post_thumbnail() ) {
				$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
				<div class="hero-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
			<?php } else { ?>
				<div class="hero-image" style="background: url( '<?php echo get_template_directory_uri(); ?>/assets/img/hero-contact.jpg' ) center center no-repeat; background-size: cover;"></div>
			<?php } ?>
		</section>
		<section class="contact-overview">
			<div class="container-desktop">
				<div class="column-container">
					<?php if ( get_the_content() ) { ?>
						<div class="column-half yellow-banner">
							<?php the_content(); ?>
						</div>
					<?php } ?>
					<div class="column-half contact-info">
						<div class="container">
							<div class="section-heading">
								<h1 class="section-heading-text">Grub Gourmet</h1>
							</div>
							<?php if ( get_field( 'google_maps_link', 'option' ) ) { ?>
								<a href="<?php the_field( 'google_maps_link', 'option' ); ?>" target="_blank">
							<?php } ?>
									<?php if ( get_field( 'street_address', 'option' ) ) { ?>
										<p><?php the_field( 'street_address', 'option' ); ?></p>
									<?php } 
									if ( get_field( 'city_state_zip', 'option' ) ) { ?>
										<p><?php the_field( 'city_state_zip', 'option' ); ?></p>
									<?php }
							if ( get_field( 'google_maps_link', 'option' ) ) { ?>
								</a>
							<?php } ?>
							<div class="social-icons">
								<?php if ( get_field( 'facebook_url', 'option' ) ) { ?>
									<a href="<?php the_field( 'facebook_url', 'option' ); ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri();?>/assets/img/social-facebook.png" alt="Facebook">
									</a>
								<?php } 
								if ( get_field( 'twitter_url', 'option' ) ) { ?>
									<a href="<?php the_field( 'twitter_url', 'option' ); ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri();?>/assets/img/social-twitter.png" alt="Twitter">
									</a>
								<?php } 
								if ( get_field( 'pinterest_url', 'option' ) ) { ?>
									<a href="<?php the_field( 'pinterest_url', 'option' ); ?>" target="_blank">
										<img src="<?php echo get_template_directory_uri();?>/assets/img/social-pinterest.png" alt="Pinterest">
									</a>
								<?php } ?>
							</div>
							<h3 class="burford-text">Need Help?</h3>
							<?php if ( get_field( 'phone_number', 'option' ) ) { ?>
								<a href="tel:<?php the_field( 'phone_number', 'option' ); ?>"><p><?php the_field( 'phone_number', 'option' ); ?></p></a>
							<?php } ?>
							<p>or</p>
							<?php if ( get_field( 'contact_email', 'option' ) ) { ?>
								<a href="mailto:<?php the_field( 'contact_email', 'option' ); ?>" class="red-link"><p><?php the_field( 'contact_email', 'option' ); ?></p></a>
							<?php } ?>
							<img src="<?php echo get_template_directory_uri();?>/assets/img/line-headings.png" class="line" alt="">
						</div>
					</div>
				</div>
			</div>
			<div id="where-to-buy" name="where-to-buy"></div>
		</section>

		<?php get_template_part( 'template-parts/content', 'where-to-buy' ); ?>

		<section class="contact-logos hide-mobile">
			<div class="inner-container column-container">
				<?php if( have_rows( 'contact_logos' ) ):
						while ( have_rows( 'contact_logos' ) ) : the_row(); ?>
							<div class="column-half">
								<a href="<?php the_sub_field( 'logo_link' ); ?>" target="_blank">
									<img src="<?php the_sub_field( 'contact_logo_image' ); ?>" alt="">
								</a>
							</div>
						<?php endwhile;
					else :
					// no rows found
				endif; ?>
			</div>
		</section>

	</div>

<?php get_footer(); ?>
