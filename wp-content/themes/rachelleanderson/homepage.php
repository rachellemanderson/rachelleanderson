<?php
/**
 * Template Name: Home
 * 
 * @package rachelle_anderson
 */

get_header(); ?>

<div class="homepage">
	<section class="home-hero no-space vertical-referance">
		<div class="hero-image"></div>
		<div class="over-text">
			<div class="outer-table">
				<div class="inner-table">
					<div class="site-logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<img src="<?php echo get_template_directory_uri(); ?>/assets/img/rachelle-anderson.png" alt="Rachelle Anderson">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="featured">
		<div class="container">
			<div class="column-container">
				<div class="column-half">
					<h2 class="heading">Resume</h2>
					<div class="preview">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg-texture.png" alt="Resume Preview">
					</div>
					<a class="button" href="#">View More</a>
				</div>
				<div class="column-half">
					<h2 class="heading">Dev Work</h2>
					<div class="preview">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/img/bg-texture.png" alt="Resume Preview">
					</div>
					<a class="button" href="#">View More</a>
				</div>
			</div>
		</div>
	</section>
	<section class="home-about">
		<div class="container">
			<h2 class="heading">About</h2>
			<img src="<?php echo get_template_directory_uri(); ?>/assets/img/about.jpg" alt="About">
			<p>My name is Rachelle Anderson. I was raised most of my life near Boulder, Colorado and now live in the Denver area. My husband and I have been married for 3 years and he is currently attending medical school at the University of Colorado. I graduated from Brigham Young University in 2013 with a degree in Mathematics. I love finding creative solutions to difficult problems!</p>
			<a class="button light" href="#">View More</a>
		</div>
	</section>
</div>

<?php get_footer(); ?>
