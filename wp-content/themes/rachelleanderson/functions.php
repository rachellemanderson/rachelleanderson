<?php
/**
 * rachelle_anderson functions and definitions
 *
 * @package rachelle_anderson
 */

define( 'ASSETS_VERSION', '1.0.0' );

if ( ! function_exists( 'rachelle_anderson_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function _vtg_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on this, use a find and replace
	 * to change 'rachelle_anderson' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'rachelle_anderson', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'rachelle_anderson' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( '_vtg_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // _vtg_setup
add_action( 'after_setup_theme', '_vtg_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function _vtg_content_width() {
	$GLOBALS['content_width'] = apply_filters( '_vtg_content_width', 640 );
}
add_action( 'after_setup_theme', '_vtg_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function _vtg_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'rachelle_anderson' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', '_vtg_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function _vtg_scripts() {
	wp_enqueue_style( 'slick-style', get_stylesheet_directory_uri() . '/assets/vendor/slick.js/slick/slick.css', array(), ASSETS_VERSION );
	wp_enqueue_style( 'slick-style-theme', get_stylesheet_directory_uri() . '/assets/vendor/slick.js/slick/slick-theme.css', array( 'slick-style' ), ASSETS_VERSION );
	wp_enqueue_style( 'fancybox-css', get_stylesheet_directory_uri() . '/assets/vendor/fancybox/source/jquery.fancybox.css', array(), ASSETS_VERSION );
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/assets/vendor/font-awesome/css/font-awesome.min.css', array(), ASSETS_VERSION );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/vendor/modernizr/modernizr.js', array(), '2.8.3', false );
	wp_enqueue_script( 'slick', get_template_directory_uri() . '/assets/vendor/slick.js/slick/slick.min.js', array( 'jquery' ), ASSETS_VERSION );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/assets/vendor/fancybox/source/jquery.fancybox.pack.js', array( 'jquery' ), ASSETS_VERSION );
	wp_enqueue_script( 'fancybox-media-js', get_template_directory_uri() . '/assets/vendor/fancybox/source/helpers/jquery.fancybox-media.js', array( 'jquery' ), ASSETS_VERSION );
	wp_enqueue_script( 'noframework.waypoints.min.js', get_template_directory_uri() . '/assets/vendor/waypoints/lib/noframework.waypoints.min.js', array( 'jquery' ), ASSETS_VERSION );
	wp_enqueue_script( 'selectboxit', get_template_directory_uri() . '/assets/vendor/selectBoxIt.js/src/javascripts/jquery.selectBoxIt.min.js', array( 'jquery', 'jquery-ui-widget' ), ASSETS_VERSION );

	if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
		wp_enqueue_style( 'rachelle_anderson-style-dev', get_stylesheet_directory_uri() . '/assets/build/styles.css', array(), ASSETS_VERSION );
		wp_enqueue_script( 'rachelle_anderson-scripts-dev', get_template_directory_uri() . '/assets/build/scripts.js', array(), ASSETS_VERSION, true );
	} else {
		wp_enqueue_style( 'rachelle_anderson-style', get_template_directory_uri() . '/assets/dist/styles.min.css', array(), ASSETS_VERSION );
		wp_enqueue_script( 'rachelle_anderson-scripts', get_template_directory_uri() . '/assets/dist/scripts.min.js', array(), ASSETS_VERSION, true );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', '_vtg_scripts' );


//
// Register Custom Post Types
//
function custom_post_type() {
	//
	// Products
	//
	$labels = array(
		'name'                => _x( 'Products', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Product', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Products', 'text_domain' ),
		'name_admin_bar'      => __( 'Products', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Event:', 'text_domain' ),
		'all_items'           => __( 'All Products', 'text_domain' ),
		'add_new_item'        => __( 'Add New Product', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Product', 'text_domain' ),
		'edit_item'           => __( 'Edit Product', 'text_domain' ),
		'update_item'         => __( 'Update Product', 'text_domain' ),
		'view_item'           => __( 'View Product', 'text_domain' ),
		'search_items'        => __( 'Search Product', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'products', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail', 'editor', 'comments' ),
		'taxonomies'          => array( ''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'menu_icon'           => 'dashicons-cart',
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'products', $args );

	//
	// Author Profiles
	//
	$labels = array(
		'name'                => _x( 'Author Profiles', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Author Profile', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Author Profiles', 'text_domain' ),
		'name_admin_bar'      => __( 'Author Profiles', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Event:', 'text_domain' ),
		'all_items'           => __( 'All Author Profiles', 'text_domain' ),
		'add_new_item'        => __( 'Add New Author Profile', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Author Profile', 'text_domain' ),
		'edit_item'           => __( 'Edit Author Profile', 'text_domain' ),
		'update_item'         => __( 'Update Author Profile', 'text_domain' ),
		'view_item'           => __( 'View Author Profile', 'text_domain' ),
		'search_items'        => __( 'Search Author Profile', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'authors', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'taxonomies'          => array( ''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'menu_icon'           => 'dashicons-id-alt',
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'authors', $args );

	//
	// Store Locations
	//
	$labels = array(
		'name'                => _x( 'Store Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Store Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Store Locations', 'text_domain' ),
		'name_admin_bar'      => __( 'Store Locations', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Event:', 'text_domain' ),
		'all_items'           => __( 'All Store Locations', 'text_domain' ),
		'add_new_item'        => __( 'Add New Store Location', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Store Location', 'text_domain' ),
		'edit_item'           => __( 'Edit Store Location', 'text_domain' ),
		'update_item'         => __( 'Update Store Location', 'text_domain' ),
		'view_item'           => __( 'View Store Location', 'text_domain' ),
		'search_items'        => __( 'Search Store Location', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'locations', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title' ),
		'taxonomies'          => array( ''),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'menu_icon'           => 'dashicons-location',
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
	);
	register_post_type( 'locations', $args );
}

//
// Hook into the 'init' action
//
add_action( 'init', 'custom_post_type', 0 );

//
// ACF Options Page
//
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

//
// Shortcode for blog article subheadings
//
add_shortcode( 'subheading', 'subheading_output' );
function subheading_output( $atts, $content ) {
	$atts = shortcode_atts( array(
		'text' => ''
	), $atts );
	return '<h2 class="burford-text">' . $atts['text'] . '</h2>';
}

/**
 * Custom walker class.
 */
class WPDocs_Walker_Nav_Menu extends Walker_Nav_Menu {

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $wp_query;
		$indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
 
		// Depth-dependent classes.
		$depth_classes = array(
			( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
			( $depth >=2 ? 'sub-sub-menu-item' : '' ),
			( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
			'menu-item-depth-' . $depth
		);
		$depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
 
		// Passed classes.
		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
 
		// Build HTML.
		$output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
 
		// Link attributes.
		$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
		$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
		$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
		$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
		$attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

		// Build HTML output and pass through the proper filter.
		$item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
			$args->before,
			$attributes,
			$args->link_before,
			apply_filters( 'the_title', $item->title, $item->ID ),
			$args->link_after,
			$args->after
		);

		$products_loop = new WP_Query( array( 'post_type' => 'products', 'posts_per_page' => -1, 'meta_key' => '', 'orderby' => 'meta_value', 'order' => 'ASC' ) );

		$customMenuItem = 
			'<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
				<a class="menu-link" href="javascript:void(0)">Products</a>
				<ul class="sub-menu menu-odd menu-depth-1"><div class="column-container">';
					
					if ( have_posts() ) :
						while ( $products_loop->have_posts() ) : $products_loop->the_post();
							$customMenuItem .= '<div class="menu-item menu-product column-half"><a href="' . get_the_permalink() . '"><div class="product-preview">' . "\n" . $indent . get_the_post_thumbnail() . '<h3 class="burford-text">' . get_the_title() . '</h3></div></a></div>';
						endwhile;
					endif; 
					wp_reset_postdata();

				$customMenuItem .= '</div></ul>
			</li>';

		static $itemnum = 0;

		if ( $depth > 0 ) {
			$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
		}
		else {
			$itemnum += 1;
			if ( $itemnum === 1 ) {
				$output = $customMenuItem . $output. apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			} else {
				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}
		}
	}
}

/**
 * Return close locations
 */
require get_template_directory() . '/inc/locations-functions.php';

/**
 * Using Wordpress comments for reviews
 */
require get_template_directory() . '/inc/review-functions.php';

/**
 * Calculates average rating for a products
 */
require get_template_directory() . '/inc/product-rating-function.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom data - Custom POst types, taxonomies, etc
 */
require get_template_directory() . '/inc/custom-data.php';