<?php
/**
 * Template part for displaying single posts.
 *
 * @package rachelle_anderson
 */

$results = showAvgRating( $post->ID );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-post-id="<?php the_ID(); ?>">
	<div class="section-heading hide-mobile">
		<h1 class="section-heading-text"><?php the_title() ?></h1>
		<div class="write-review">
			<?php echo $results[0]; ?>
			<p class="link-reviews js-to-anchor">Write a Review</p>
		</div>
	</div>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'rachelle_anderson' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php _s_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

