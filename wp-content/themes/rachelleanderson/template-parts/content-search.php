<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rachelle_anderson
 */

?>

<article id="post-<?php the_ID(); ?>">
	<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
			<div class="post-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
		<?php  } ?>
		<h2 class="burford-text"><?php the_title(); ?></h2>
	</a>
	<p class="burford-text article-date"><?php the_modified_date( 'm.d.Y' ); ?></p>
	<p class="article-author">By: <a href=""><?php the_author(); ?></a></p>
	<?php the_excerpt(); ?>
</article><!-- #post-## -->


