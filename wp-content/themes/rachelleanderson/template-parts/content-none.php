<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package rachelle_anderson
 */

?>

<section class="no-results">
	<header class="page-header">
		<h1 class="section-heading-text"><?php esc_html_e( 'Nothing Found', 'rachelle_anderson' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'There are no posts that meet this criteria.', 'rachelle_anderson' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'rachelle_anderson' ); ?></p>

		<?php else : ?>

			<p><?php esc_html_e( 'There are no posts that meet this criteria', 'rachelle_anderson' ); ?></p>

		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
