<?php
/**
 * Template part for displaying posts.
 *
 * @package rachelle_anderson
 */

?>

<article id="post-<?php the_ID(); ?>">
	<a href="<?php the_permalink(); ?>">
		<h2 class="italic-heading underline-heading"><?php the_title(); ?></h2>
	</a>
	<p class="date"><?php the_date(); ?></p>
	<a href="<?php the_permalink(); ?>">
		<?php if ( has_post_thumbnail() ) {
			$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
			<div class="post-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
		<?php  } ?>
	</a>
	<?php the_excerpt(); ?>
	<center><a class="button-bars" href="#">Read More</a></center>
</article><!-- #post-## -->
