<?php
/**
 * Template part for displaying single posts.
 *
 * @package rachelle_anderson
 */
?>

<article id="post-<?php the_ID(); ?>">
	<div class="section-heading">
		<div class="title-date">
			<h1 class="section-heading-text"><?php the_title(); ?></h1>
			<p class="burford-text article-date"><?php the_modified_date( 'm.d.Y' ); ?></p>
		</div>
		<div class="social-sharing">
			<a href="mailto:?subject=<?php the_title(); ?>&amp;body=<?php the_title(); ?>: <?php the_permalink(); ?>" title="Share by Email">
				<img src="<?php echo get_template_directory_uri();?>/assets/img/social-email.png" alt="Email">
			</a>
			<a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" title="Share to Facebook" target="_blank">
				<img src="<?php echo get_template_directory_uri();?>/assets/img/social-facebook.png" alt="Facebook">
			</a>
			<a href="//twitter.com/home?status=<?php the_title(); ?>: <?php the_permalink(); ?> %23sharegrub" title="Share to Twitter" target="_blank">
				<img src="<?php echo get_template_directory_uri();?>/assets/img/social-twitter.png" alt="Twitter">
			</a>
			<a href='//pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php the_post_thumbnail_url(); ?>&amp;description=<?php the_title(); ?>;' title="Share to Pinterest" target="_blank">
				<img src="<?php echo get_template_directory_uri();?>/assets/img/social-pinterest.png" alt="Pinterest">
			</a>
		</div>
	</div>
	<?php if ( get_field( 'article_author' ) ) { ?>
		<div class="article-author">
			<?php 
				$post_field = get_field( 'article_author' );
				$auth_post_id = get_post( $post_field );
				$src = wp_get_attachment_image_src( get_post_thumbnail_id( $auth_post_id ), array( 5600, 1000 ), false, '' );
			if ( $src != '' ) { ?>
				<div class="author-image" style="background: url(<?php echo $src[0]; ?>) center center no-repeat; background-size: cover;"></div>
			<?php } ?>
			<p>By: 
				<?php if ( get_field( 'author_link', $auth_post_id ) ) { ?>
					<a href="<?php the_field( 'author_link', $auth_post_id ); ?>" target="_blank" class="red-link">
				<?php } 
					echo get_the_title( $auth_post_id );
				if ( get_field( 'author_link', $auth_post_id ) ) { ?>
					</a>
				<?php } ?>
			</p>
		</div>
	<?php } ?>
	<?php the_content(); ?>
</article><!-- #post-## -->
