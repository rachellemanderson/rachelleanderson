<?php
/**
 * Template part for displaying posts.
 *
 * @package rachelle_anderson
 */

?>

<div class="column-half">
	<a href="<?php the_permalink(); ?>">
		<div class="product-preview <?php if ( ! has_post_thumbnail() ) { ?>no-prod-image extra-padding<?php } ?>">
			<?php if ( has_post_thumbnail() ) {
				the_post_thumbnail(); 
			} ?>
			<h3 class="burford-text"><?php the_title() ?></h3>
		</div>
	</a>
	<a href="<?php the_permalink(); ?>" class="button">
		<span>View Product</span>
	</a>
</div>