<?php
/**
 * Template part for displaying posts.
 *
 * @package rachelle_anderson
 */

?>

<div class="fade-in js-fade-map" id="js-fade-map">
	<section class="where-to-buy no-space">
		<div>
			<div class="column-container">
				<div class="column-half map-frame"><div id="map"></div></div>
				<div class="column-half extra-padding">
					<div class="container">
						<div class="section-heading">
							<h2 class="section-heading-text">Where to Buy</h2>
						</div>
						<form id="form">
							<input id="addressInput" class="js-no-results" type="text" name="fname" placeholder="Enter Zip Code">
							<span class="js-active-results">
								<select id="locationSelect"></select>
							</span>
							<p class="address-validation"></p>
							<div class="button js-no-results">
								<input class="submit" type="submit" value="Search">
							</div>
						</form>
						<a href="javascript:void(0)" class="button clear-search js-clear-search js-active-results">
							<span>Clear Search</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
	var ajaxurl = <?php echo '"' . admin_url( 'admin-ajax.php' ) . '";'; ?>
</script>

<script>
	var $ = jQuery,
		map,
		markers = [],
		infoWindow,
		locationSelect,
		assetURL = $( 'body' ).data( 'asset-url' );
		
	function load() {
		map = new google.maps.Map( document.getElementById( 'map' ), {
			center: { lat: 40.4292705, lng: -104.9033453 },
			zoom: 10,
			disableDefaultUI: true,
			scrollwheel: false,
			zoomControl: true
		});

		infoWindow = new google.maps.InfoWindow();
		infoWindow = new google.maps.InfoWindow();

		locationSelect = document.getElementById( 'locationSelect' );
		locationSelect.onchange = function() {
			var markerNum = locationSelect.options[ locationSelect.selectedIndex ].value;
			if ( markerNum != 'none' ) {
				google.maps.event.trigger( markers[ markerNum ], 'click' );
			}
		};
	}

	$( '#form' ).on( 'submit', function( e ) {
		e.preventDefault();
		$( '.address-validation' ).html( '' );
		var zipcode = document.getElementById( 'addressInput' ).value;
		searchLocations( zipcode );
	});

	$( '.js-clear-search' ).on( 'click', function() {
		clearLocations();
		$( '.address-validation' ).html( '' );
		$( '.js-no-results' ).css({ 'display': 'inline-block' });
		$( '.js-active-results' ).hide();
		$( '#addressInput' ).val('');
		$( 'select' ).data( 'selectBox-selectBoxIt' ).refresh();
	});

	function searchLocations( zip ) {
		var address = zip;
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode( { address: address }, function( results, status ) {
			if ( status == google.maps.GeocoderStatus.OK ) {
				searchLocationsNear( results[0].geometry.location );
			} else {
				$( '.address-validation' ).append( 'Looks like ' + address + ' is off the grid. Try something else?' );
			}
		});
	}

	function clearLocations() {
		infoWindow.close();

		for ( var i = 0; i < markers.length; i++ ) {
			markers[i].setMap( null );
		}

		markers.length = 0;
		locationSelect.innerHTML = "";
		var option = document.createElement( 'option' );
		option.value = 'none';
		option.innerHTML = 'See all results:';
		locationSelect.appendChild( option );
	}

	function searchLocationsNear( center ) {
		clearLocations();

		var radius = 25;
		$.ajax ( {
			method: 'POST',
			url: ajaxurl,
			data:     {
				action: 'getlocations',
				centerlat: center.lat(),
				centerlng: center.lng(), 
				radius: radius
			},
			error: function( xhr, ajaxOptions, thrownError ) {
				console.log( "failure " + thrownError );
			},
			success: function( result ) {
				var xml = result;
				var markerNodes = xml.documentElement.getElementsByTagName( 'marker' );
				var bounds = new google.maps.LatLngBounds();
				for ( var i = 0; i < markerNodes.length; i++ ) {
					var name = markerNodes[i].getAttribute( 'name' );
					var address = markerNodes[i].getAttribute( 'address' );
					var distance = parseFloat( markerNodes[i].getAttribute( 'distance' ) );
					var latlng = new google.maps.LatLng(
						parseFloat( markerNodes[i].getAttribute( 'lat' ) ),
						parseFloat( markerNodes[i].getAttribute( 'lng' ) )
					);

					createOption( name, distance, i);
					createMarker( latlng, name, address );
					bounds.extend( latlng );
				}

				if ( markerNodes.length === 0 ) {
					$( '.address-validation' ).append( 'How hungry are you? Widen your search to find deliciousness.' );
					map.setCenter( { lat: center.lat(), lng: center.lng() } ); 
					$( '.js-clear-search' ).css({ 'display': 'inline-block' });
				} else {
					map.fitBounds( bounds );
					$( '.js-active-results' ).css({ 'display': 'inline-block' });
				}
				locationSelect.style.visibility = 'visible';
				$( '.js-no-results' ).hide();
				locationSelect.onchange = function() {
					var markerNum = locationSelect.options[ locationSelect.selectedIndex ].value;
					google.maps.event.trigger( markers[markerNum], 'click' );
				};
			}
		} );
	}

	function createMarker( latlng, name, address ) {
		var html = '<span class="burford-text">' + name + '</span><br/>' + address;
		var marker = new google.maps.Marker({
			map: map,
			position: latlng,
			icon: assetURL + '/assets/img/location-pin.png'
		});
		google.maps.event.addListener( marker, 'click', function() {
			infoWindow.setContent( html );
			infoWindow.open( map, marker );
		});
		markers.push( marker );
	}

	function createOption( name, distance, num ) {
		var option = document.createElement( 'option' );
		option.value = num;
		option.innerHTML = name + ' (' + distance.toFixed(1) + 'mi)';
		locationSelect.appendChild( option );
		$( 'select' ).data( 'selectBox-selectBoxIt' ).refresh();
	}

	function parseXml( str ) {
		if ( window.ActiveXObject ) {
			var doc = new ActiveXObject( 'Microsoft.XMLDOM' );
			doc.loadXML( str );
			return doc;
		} else if ( window.DOMParser ) {
			return ( new DOMParser() ).parseFromString( str, 'text/xml' );
		}
	}
</script>

<script src="https://maps.googleapis.com/maps/api/js?callback=load" async defer></script>
