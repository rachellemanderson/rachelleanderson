( function( $ ) {

	// Fire it up
	var RA = {
		challengeElement: null,
		context: null,

		/**
		 * Initialize site
		 */
		init: function() {
			/**
			 * Set the initial breakpoint context
			 */
			RA.challengeElement = document.querySelector( '.breakpoint-context' );
			RA.challengeContext();
			RA.toggleMobileMenu();
			RA.initSliders();
			RA.initWaypoints();
			RA.triggerPostFilters();
			RA.scrollToAnchor();
			RA.loadMoreReviews();

			/**
			 * Check breakpoint context on window resizing
			 * Throttled/debounced for better performance
			 */
			$( window ).resize( RA.debounce( function() {
				RA.challengeContext();
			}, 100 ) );

			$( window ).on( 'scroll', function() {
			} );
		},

		/**
		 * Device targeting should be based on media queries in CSS,
		 * we do not define this in scripts
		 * Modified from http://davidwalsh.name/device-state-detection-css-media-queries-javascript
		 */
		challengeContext: function() {
			var styles = window.getComputedStyle( RA.challengeElement ),
				index = parseInt( styles.getPropertyValue( 'z-index' ), 10 ),
				states = {
					1: 'mobile',
					2: 'tablet'
				};

			RA.context = states[ index ] || 'desktop';
		},

		toggleMobileMenu: function() {
			// $( '.js-nav-icon' ).on( 'click', function() {
			// 	$( '.header-bar' ).fadeToggle( '500' );
			// 	$( '.mobile-header-bar, .header-bar' ).toggleClass( 'nav-open' );
			// 	$( 'body' ).toggleClass( 'prevent-scroll' );
			// });
		},

		//Slick Sliders
		initSliders: function() {
			$( '.js-home-slider' ).slick( {
				dots: true,
				infinite: true,
				arrows: true,
				speed: 500,
				autoplay: false,
				cssEase: 'linear',
			} );
		},

		initWaypoints: function() {

		},

		/**
		 * Throttle/debounce helper
		 * Modified from http://remysharp.com/2010/07/21/throttling-function-calls/
		 */
		debounce: function( fn, delay ) {
			var timer = null;

			return function() {
				var context = this,
					args = arguments;

				clearTimeout( timer );

				timer = setTimeout( function() {
					fn.apply( context, args );
				}, delay );
			};
		},

		buildFilteredURLs: function( selected ) {			
			// var blogURL = $( '.blog-archive' ).data( 'blog-url' ),
			// 	currentFilterType = selected.data( 'filter' ),
			// 	query = window.location.search.slice( 1 ),
			// 	tagsPresent = false,
			// 	newFilter;

			// var blogParameters = [];

			// queryArray = query.split( '&' );

			// for ( var i = 0; i < queryArray.length; i++ ) {
			// 	var key = queryArray[i].split( '=' ).shift().toLowerCase();
			// 	var	value = queryArray[i].split( '=' ).pop();

			// 	if ( ( currentFilterType === 'tags' || currentFilterType === 'year' || currentFilterType === 'search' ) && key === 'page' ) {
			// 		blogParameters.push( 'page=1' );
			// 	} else if ( currentFilterType === 'remove-tag' && key === 'tags' ) {
			// 		//Don't add tag filters
			// 	} else if ( currentFilterType === 'remove-search' && key === 'search' ) {
			// 		//Don't add search filters
			// 	} else if ( currentFilterType !== key ) {
			// 		blogParameters.push( queryArray[i] );
			// 	}
			// }

			// if ( currentFilterType === 'tags' || currentFilterType === 'remove-tag' ) {

			// 	if ( currentFilterType === 'tags' ) {
			// 		selectedTag = $( '.filter-by-tags option:selected' ).val();
			// 	} else {
			// 		selectedTag = selected.data( 'remove-tag' );
			// 	}

			// 	// Add any existing tags from the query to newFilter
			// 	for ( var j = 0; j < queryArray.length; j++ ) {
			// 		var tagkey = queryArray[j].split( '=' ).shift().toLowerCase();
			// 		var	tagvalue = queryArray[j].split( '=' ).pop();

			// 		if ( tagkey === 'tags' ) {
			// 			var valueArray = tagvalue.split( ',' );

			// 			for ( var k = 0; k < valueArray.length; k++ ) {
			// 				if ( selectedTag !== valueArray[k] && tagsPresent ) {
			// 					newFilter += ',' + valueArray[k];
			// 				} else if ( selectedTag !== valueArray[k] ) {
			// 					newFilter = 'tags=' + valueArray[k];
			// 					tagsPresent = true;
			// 				}
			// 			}
			// 		}
			// 	}

			// 	// Add the new tag to newFilter
			// 	if ( tagsPresent && currentFilterType === 'tags' ) {
			// 		newFilter += ',' + selectedTag;
			// 	} else if ( currentFilterType === 'tags' ) {
			// 		newFilter = 'tags=' + selectedTag;
			// 	}
			// } else if ( currentFilterType === 'order' ) {
			// 	newFilter = 'order=' + $( '.sort-by-date option:selected' ).val();
			// } else if ( currentFilterType === 'page' ) {
			// 	newFilter = 'page=' + selected.html();
			// } else if ( currentFilterType === 'year' ) {
			// 	newFilter = 'year=' + selected.html();
			// } else if ( currentFilterType === 'search' ) {
			// 	newFilter = 'search=' + selected.find( '.search-field' ).val();
			// } 

			// blogParameters.push( newFilter );
			// blogParameters = blogParameters.join( '&' );

			// window.location.href = blogURL + '?' + blogParameters;
		},

		scrollToAnchor: function() {
			$( '.js-to-anchor' ).on( 'click', function() {
				var	indexNumber = $( '.js-to-anchor' ),
					anchor = $( '.js-anchor' ),
					offset = 0;

				if ( RA.context === 'desktop' ) {
					offset = 87;
				}
					
				$( 'body, html' ).animate( { 
					scrollTop: anchor.offset().top - offset
				}, 800 );
			});
		},

		loadMoreReviews: function() {
			$( '.js-load-reviews' ).on( 'click', function() {
				$.ajax ( {
					method: 'POST',
					url: ajaxurl,
					data:     {
						action: 'getmorereviews',
						offset: $( '.list-reviews' ).data( 'review-offset' ) + 3,
						postID: $( 'article' ).data( 'post-id' )
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( "failure " + thrownError );
					},
					success: function( result ) {
						var offsetAmt = $( '.list-reviews' ).data( 'review-offset' ) + 3;
						$( '.list-reviews' ).data( 'review-offset', offsetAmt );

						var comments = JSON.parse( result ),
							arrayLength = comments.length;

						for( var i = 0; i < arrayLength; i++ ) {
							var reviewHTML = '<div class="column-third"><h3 class="burford-text blue">';
							reviewHTML += comments[i][1][0];
							reviewHTML += '</h3><div class="star-rating">';

							var productRating = comments[i][2][0];
							for ( j = 1; j <= 5; j++ ) { 
								if ( j <= productRating ) {
									reviewHTML += '<div class="star full"></div>';
								} else {
									reviewHTML += '<div class="star"></div>';
								}
							}

							reviewHTML += '</div><p class="reviewer">';
							reviewHTML += comments[i][0].comment_author + ' ' + comments[i][3][0];
							reviewHTML += '<span>' + comments[i][4] + '</span></p>';
							reviewHTML += '<p>' + comments[i][0].comment_content + '</p></div>';

							$( reviewHTML ).appendTo( '.list-reviews' ).hide().fadeIn( 'slow' );
						}
					}
				} );

				// Check if load more button should be displayed each time more items are loaded
				if ( $( '.total-reviews span' ).text() <= $( '.list-reviews .column-third' ).length + 3 ) {
					$( '.js-load-reviews' ).fadeOut();
				}
			});

			// Check if load more button should be displayed on page load
			if ( $( '.total-reviews span' ).text() <= $( '.list-reviews .column-third' ).length ) {
				$( '.js-load-reviews' ).hide();
			}
		}
	};

	$( document ).ready( function() {
		RA.init();
	});
	// Chain any click events here

} ) ( jQuery );