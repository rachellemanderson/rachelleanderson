<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package rachelle_anderson
 */

get_header(); ?>


	<div class="default">
		<section class="no-space">
			<?php if ( has_post_thumbnail() ) {
				$src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), array( 5600,1000 ), false, '' ); ?>
				<div class="hero-image" style="background: url(<?php echo $src[0]; ?> ) center center no-repeat; background-size: cover;"></div>
			<?php  } else { ?> 
				<div class="header-padding"></div>
			<?php } ?>
		</section>
		<section class="main-content">
			<div class="inner-container">
				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/content', 'page' ); ?>

					<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

				<?php endwhile; // End of the loop. ?>
			</div>
		</section>
	</div>

<?php get_footer(); ?>
